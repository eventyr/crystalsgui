
from tkinter import *
from tkinter import ttk

root = Tk()

msg = StringVar()
a1 = StringVar()
a2 = StringVar()
a3 = StringVar()

Label(root, text="Please enter a, b, c, α, β, γ (in °), separated\
 by spaces.").pack()

y = Entry(root, width=50); y.pack()

def get_a1_a2_a3():
    """
    INPUTS: a, b, c, alpha, beta, gamma from textbox.
    OUTPUT: Computes lattice vectors and prints them to appropriate boxes.
    """
    try:
        a, b, c, alpha, beta, gamma = y.get().split()
        a = float(a); b = float(b); c = float(c)
        alpha = float(alpha); beta = float(beta); gamma = float(gamma)
        msg.set("Successfully computed lattice vectors")

        # TODO: Print the correct stuff to the boxes.
        a1.set("Hello, please fill me in.") 
        a2.set("Hello, please fill me in.") 
        a3.set("Hello, please fill me in.") 

    except ValueError:
        # Exception handling is always a good idea.
        msg.set("Something's not right; please check input :(")

Label(root, width=50, textvariable=msg).pack()
Button(root, text="Compute vectors",command=get_a1_a2_a3).pack()

Label(root, text = "Vector a1:").pack()
Entry(root, width=50, textvariable=a1).pack()

Label(root, text = "Vector a2:").pack()
Entry(root, width=50, textvariable=a2).pack()

Label(root, text = "Vector a3:").pack()
Entry(root, width=50, textvariable=a3).pack()

### END CODE HERE

root.mainloop()
